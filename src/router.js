import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import Hiburan from './views/Hiburan.vue'
import Kesehatan from './views/Kesehatan.vue'
import Ilmu from './views/Ilmu.vue'
import Olah from './views/Olah.vue'

Vue.use(Router)

export default new Router({
  mode: process.env.CORDOVA_PLATFORM ? 'history' : 'history',
  base: process.env.BASE_URL,
  linkExactActiveClass: "active",
  scrollBehavior: () => ({ y: 0 }),
  saveScrollPosition: true,    
  routes: [
    {
      path: '/',
      redirect: '/home',      
      name: '/',
      component: Home,
      children:[
        {
          path: '/home',
          name: 'home',
          component: Home,
          
        }
      ]           
    },
    {
      path: '/about',
      name: 'about',
      component: About    
    },
    {
      path: '/hiburan',
      name: 'hiburan',
      component: Hiburan    
    },    
    {
      path: '/kesehatan',
      name: 'kesehatan',
      component: Kesehatan    
    },
    {
      path: '/ilmu',
      name: 'ilmu',
      component: Ilmu    
    },    
    {
      path: '/olah',
      name: 'olah',
      component: Olah    
    }        
  ]
})
